// main.js //

let boardModel = [
    [null, null, null, null, null, null, null],
    [null, null, null, null, null, null, null],
    [null, null, null, null, null, null, null],
    [null, null, null, null, null, null, null],
    [null, null, null, null, null, null, null],
    [null, null, null, null, null, null, null]
]
let currentPlayer = 1
let numberOfDiscsPlayed = 0

const displayMessage = function (message) {
    document.getElementById("message").innerHTML = '<p>' + message + '</p>';
    console.log(message) 
}
const displayCurrentPlayer = function (currPlayer) {
    displayMessage('<em>' + "Who's turn is it?" + '</em>' + " Player " + currPlayer)
    
}
const displayWhoWon = function (winner) {
    displayMessage("Winner is player" + winner)
}
const displayTieMessage = function () {
    displayMessage("Tie game!")
}

const columnIsFull = function (board, index) {
    return board[0][index] !== null
}

const dropDiskIntoColumn = function (columnEl, board, playerNum) {
    const columnIndex = Number(columnEl.id.slice(-1))
    if (columnIsFull(board, columnIndex)) {
        return
    }
    // update the boardModel
    for (let row=board.length-1; row>=0; row--) {
        if (board[row][columnIndex] === null) {
            board[row][columnIndex] = playerNum
            break
        }
    }
    // update the HTML
    if (currentPlayer === 1) {
        let newDisc = document.createElement('div')
        newDisc.className = (playerNum === 1) ? "disc pl1" : "disc pl2"
        columnEl.appendChild(newDisc)
        return currentPlayer = 2
    }
    if (currentPlayer === 2) {
        let newDisc = document.createElement('div')
        newDisc.className = (playerNum === 1) ? "disc pl1" : "disc pl2"
        columnEl.appendChild(newDisc)
        return currentPlayer = 1
    }
}

const winnerHorizontal = function (board) {
    for (let row = 0; row < 6; row++) {
        for (let col = 0; col < board[row].length; col++) {
            if ((board[row][col] === board[row][col + 1]) &&
                (board[row][col] === board[row][col + 2]) &&
                (board[row][col] === board[row][col + 3]) &&
                (board[row][col] !== null)) {
                return board[row][col]
                
            }
        }
    }
    // return 1, 2, or null
    return null
}
const winnerVertical = function (board) {
    for (let row = 0; row < 3; row++) {
        for (let col = 0; col < board[row].length; col++) {
            if ((board[row][col] === board[row + 1][col]) &&
                (board[row][col] === board[row + 2][col]) &&
                (board[row][col] === board[row + 3][col]) &&
                (board[row][col] !== null)) {
                return board[row][col]
                
            }
        }
    }
    // return 1, 2, or null
    return null
}
const winnerDiagonalDownRight = function (board) {
    for (let row = 0; row < 3; row++) {
        for (let col = 0; col < board[row].length; col++) {
            if ((board[row][col] === board[row + 1][col + 1]) &&
                (board[row][col] === board[row + 2][col + 2]) &&
                (board[row][col] === board[row + 3][col + 3]) &&
                (board[row][col] !== null)) {
                return board[row][col]
                
            }
        }
    }
    // return 1, 2, or null
    return null
}
const winnerDiagonalUpRight = function (board) { 
    for (let row = 0; row < 3; row++) {
        for (let col = 0; col < board[row].length; col++) {
            if ((board[row][col + 3] === board[row + 1][col + 2]) &&
                (board[row][col + 3] === board[row + 2][col + 1]) &&
                (board[row][col + 3] === board[row + 3][col]) &&
                (board[row][col + 3] !== null)) {
                return board[row][col + 3]
                
            }
        }
    }
    // return 1, 2, or null
    return null
}

const determineGameWinner = function (board) { // pure function
    const horz = winnerHorizontal(board)
    const vert = winnerVertical(board)
    const dnrt = winnerDiagonalDownRight(board)
    const uprt = winnerDiagonalUpRight(board)
    let winner

    if (horz !== null) {
        winner = horz
    } else if (vert !== null) {
        winner = vert
    } else if (dnrt !== null) {
        winner = dnrt
    } else if (uprt !== null) {
        winner = uprt
    } else {
        winner = null
    }

    // return 1, 2, or null (tie or game isn't isn't over)
    return winner
}

const gameIsATie = function (board) {
    if (numberOfDiscsPlayed === 42) {
        return true
    } else if (numberOfDiscsPlayed !== 42) {
        return false
    }
}

const columnClickHandler = function (event) {
    console.log('click!')
    const columnThatWasClicked = event.currentTarget
    dropDiskIntoColumn(columnThatWasClicked, boardModel, currentPlayer)
    // see if the game has been won or tied
    let winner = determineGameWinner(boardModel)
    if (winner !== null) {
        displayWhoWon(winner)
    } else if (gameIsATie(boardModel)) {
        displayTieMessage()
    } else {
        displayCurrentPlayer(currentPlayer)
    }
    numberOfDiscsPlayed++

    console.log(numberOfDiscsPlayed)
}

const createColumnEventListeners = function () {
    document.querySelector('#col0').addEventListener('click', columnClickHandler)
    document.querySelector('#col1').addEventListener('click', columnClickHandler)
    document.querySelector('#col2').addEventListener('click', columnClickHandler)
    document.querySelector('#col3').addEventListener('click', columnClickHandler)
    document.querySelector('#col4').addEventListener('click', columnClickHandler)
    document.querySelector('#col5').addEventListener('click', columnClickHandler)
    document.querySelector('#col6').addEventListener('click', columnClickHandler)
}

const displayBoard = function (boardModel) {

}

const initializeGame = function () {
    displayBoard(boardModel)
    createColumnEventListeners()
    displayCurrentPlayer(currentPlayer)
}

initializeGame()




const test = function () {
    console.assert((winnerVertical([
        [null, null, null, null, null, null, null],
        [null, null, null, null, null, null, null],
        [null, null, null, null, null, null, null],
        [null, null, null, null, null, null, null],
        [null, null, null, null, null, null, null],
        [null, null, null, null, null, null, null]
    ]) === null), "Winner Vertical fails on empty board")
    console.assert((winnerVertical([
        [null, null, null, null, null, null, null],
        [null, null, null, null, null, null, null],
        [   2, null, null, null, null, null, null],
        [   2, null, null, null, null, null, null],
        [   2, null, null, null, null, null, null],
        [   2, null, null, null, null, null, null]
    ]) === 2), "Winner Vertical fails on col 0 player 2 win")
    console.assert((winnerVertical([
        [null, null, 2, null, null, null, null],
        [null, null, 1, null, null, null, null],
        [   2, null, 1, null, null, null, 1],
        [   1, null, 1, null, null, null, 1],
        [   2, null, 2, null, null, null, 1],
        [   2, null, 2, null, null, null, 1]
    ]) === 1), "Winner Vertical fails on col 6 player 1 win")
    console.assert((winnerVertical([
        [null, null, 1, null, null, null, null],
        [null, null, 1, null, null, null, null],
        [   2, null, 1, null, null, null, 1],
        [   1, null, 1, null, null, null, 2],
        [   2, null, 2, null, null, null, 1],
        [   2, null, 2, null, null, null, 1]
    ]) === 1), "Winner Vertical fails on col 2 player 1 win")
    console.assert((columnIsFull([
        [null, null, 1, null, null, null, null],
        [null, null, 1, null, null, null, null],
        [   2, null, 1, null, null, null, 1],
        [   1, null, 1, null, null, null, 2],
        [   2, null, 2, null, null, null, 1],
        [   2, null, 2, null, null, null, 1]
    ], 0) === false), "columnIsFull fails checking a partially filled col 0")
    console.assert((columnIsFull([
        [null, null, 1, null, null, null, null],
        [null, null, 1, null, null, null, null],
        [   2, null, 1, null, null, null, 1],
        [   1, null, 1, null, null, null, 2],
        [   2, null, 2, null, null, null, 1],
        [   2, null, 2, null, null, null, 1]
    ], 1) === false), "columnIsFull fails checking an empty col1")
    console.assert((columnIsFull([
        [null, null, 1, null, null, null, null],
        [null, null, 1, null, null, null, null],
        [   2, null, 1, null, null, null, 1],
        [   1, null, 1, null, null, null, 2],
        [   2, null, 2, null, null, null, 1],
        [   2, null, 2, null, null, null, 1]
    ], 2) === true), "columnIsFull fails checking a full col2")
}
//test()
